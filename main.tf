# main.tf

resource "aws_s3_bucket" "terraform_state" {
  bucket = var.s3_bucket_name
  acl    = var.s3_bucket_acl
  tags   = var.s3_bucket_tags

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
         sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_state" {

  name           = var.dynamodb_table_name
  hash_key       = "LockID"
  read_capacity  = var.dynamodb_table_capacity
  write_capacity = var.dynamodb_table_capacity
  tags           = var.dynamodb_table_tags

  attribute {
    name = "LockID"
    type = "S"
  }
}
