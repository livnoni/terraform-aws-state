# outputs.tf

output "s3_bucket_id" {
  value       = aws_s3_bucket.terraform_state.id
  description = "S3 bucket identifier (same as name)"
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.terraform_state.arn
  description = "S3 bucket ARN"
}

output "dynamodb_table_id" {
  value       = aws_dynamodb_table.terraform_state.id
  description = "DynamoDB table identifier (same as name)"
}

output "dynamodb_table_arn" {
  value       = aws_dynamodb_table.terraform_state.arn
  description = "DynamoDB table ARN"
}
