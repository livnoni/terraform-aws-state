# variables.tf

variable "s3_bucket_name" {
  description = "S3 bucket name."
  type        = string
}

variable "s3_bucket_acl" {
  description = "S3 bucket ACL."
  type        = string
  default     = "private"
}

variable "s3_bucket_tags" {
  description = "Tags for S3 bucket."
  type        = map(string)
  default     = {}
}

variable "dynamodb_table_name" {
  description = "DynamoDB table name."
  type        = string
}

variable "dynamodb_table_capacity" {
  description = "DynamoDB table read and write capacity."
  type        = number
  default     = 5
}

variable "dynamodb_table_tags" {
  description = "Tags for DynamoDB table."
  type        = map(string)
  default     = {}
}
